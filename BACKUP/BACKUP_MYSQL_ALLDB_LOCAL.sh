#!/bin/bash
#DATA STRUCTURE
mkdir -p /RACKBACKUP/RESTORE
mkdir -p /RACKBACKUP/BACKUP/WEEKLY
mkdir -p /RACKBACKUP/BACKUP/MONTHLY
mkdir -p /RACKBACKUP/BACKUP/DAILY
mkdir -p /RACKBACKUP/BACKUP/DAILY/LOG/
mkdir -p /RACKBACKUP/BACKUP/DAILY/Sunday
mkdir -p /RACKBACKUP/BACKUP/DAILY/Monday
mkdir -p /RACKBACKUP/BACKUP/DAILY/Tuesday
mkdir -p /RACKBACKUP/BACKUP/DAILY/Wednesday
mkdir -p /RACKBACKUP/BACKUP/DAILY/Thursday
mkdir -p /RACKBACKUP/BACKUP/DAILY/Friday
mkdir -p /RACKBACKUP/BACKUP/DAILY/Saturday
cat /RACKBACKUP/BACKUP/DAILY/LOG/output.log
cat /RACKBACKUP/BACKUP/DAILY/LOG/error.log
cat /RACKBACKUP/BACKUP/DAILY/LOG/data.log
cat /RACKBACKUP/BACKUP/DAILY/LOG/email.log
#VARIABLES
SERVER="BACKUP"
BACKUPDIR="/RACKBACKUP/BACULA/$SERVER"
DAYOFWEEK=$(date +"%A")
TARGET="/RACKBACKUP/BACKUP/DAILY/"
LOG="/RACKBACKUP/BACKUP/DAILY/BACKUP/DAILY/LOG/"
OUTPUT="/RACKBACKUP/BACKUP/DAILY/LOG/output.log"
ERROR="/RACKBACKUP/BACKUP/DAILY/LOG/error.log"
DATA_AFTER="/RACKBACKUP/BACKUP/DAILY/LOG/data.log"
EMAIL="/RACKBACKUP/BACKUP/DAILY/LOG/email.log"
DATA_BEFORE="/RACKBACKUP/BACKUP/DAILY/LOG/databefore.log"
EMAIL_PERSON_1="cward@fast-growing-trees.com"
EMAIL_PERSON_2="jchadward@gmail.com"
EMAIL_PERSON_3="pmarusich@fast-growing-trees.com"

#Beginning of script
touch $EMAIL
touch $DATA_BEFORE
touch $OUTPUT
touch $DATA_AFTER
touch $ERROR
echo "$(hostname)"
clear
#OUTPUT TO SCREEN
/bin/ls -lh $TARGET$DAYOFWEEK > $DATA_BEFORE
echo                                         
echo                                         
echo                                         
echo                                         
echo TODAY IS $DAYOFWEEK 
echo "THE DATE IS `date`"  
echo "SERVER IS: [ $(hostname)]"
echo "++++++++++++    BACKUP REPORT   +++++++++++++"
echo                                         
echo                                         
echo CONTENTS OF DATA THAT IS TO BE DELETED 
echo ====================================== 
cat  $DATA_BEFORE                           
echo                                         
echo                                         
echo                                       
echo THE FILES THAT GOT BACKED UP ARE       
echo ======================================= 
cat $DATA_AFTER                             
clear
#OUTPUT TO EMAIL
#/bin/ls -lh $TARGET$DAYOFWEEK > $DATA_BEFORE
echo "TODAY IS $DAYOFWEEK" >> $EMAIL
echo "THE DATE IS `date`" >> $EMAIL
echo "SERVER IS: [ $(hostname)]"  >> $EMAIL
echo "                                       " >> $EMAIL
echo "                                       " >> $EMAIL
echo "++++++++++++    BACKUP REPORT   +++++++++++++" >> $EMAIL
echo "                                       " >> $EMAIL
echo "                                       " >> $EMAIL
echo "CONTENTS OF DATA THAT IS TO BE DELETED"  >> $EMAIL
echo "======================================"  >> $EMAIL
cat $DATA_BEFORE                               >> $EMAIL
echo "                                       " >> $EMAIL
echo "                                       " >> $EMAIL
echo "                                       " >> $EMAIL
echo "THE FILES THAT GOT BACKED UP  ARE      " >> $EMAIL
echo "=======================================" >> $EMAIL
cat $DATA_AFTER                             >> $EMAIL
sleep 5
/bin/ls -lh $TARGET$DAYOFWEEK > $DATA_BEFORE
/usr/bin/find $TARGET$DAYOFWEEK -type f -name '*MYSQLDUMP_ALL_DB_BACKUP_*.gz' -exec rm -f {} \;
mysqldump --all-databases -u bacula  | gzip -9 > /RACKBACKUP/BACKUP/DAILY/$DAYOFWEEK/MYSQLDUMP_ALL_DB_BACKUP_$(date +%Y-%m-%d-%H.%M.%S).sql.gz
#service mysqld stop
#rsync -zvr /var/lib/mysql/ /RACKBACKUP/BACKUP/DAILY/TEMP
#tar cvzf /RACKBACKUP/BACKUP/DAILY/TEMP/ | gzip -9 > /RACKBACKUP/BACKUP/DAILY/ALL_DATABASE_FILES$(date +%Y-%m-%d-%H.%M.%S).tar.gz
cd /RACKBACKUP/BACKUP/DAILY/$DAYOFWEEK/
#CLEAN TODAY'S BACKUP DIR BEFORE BACKUP

#/usr/bin/find $TARGET$DAYOFWEEK -type f -name '*ALL_DATABASE_FILES_*.gz' -exec rm -f {} \;
#tar -cvzf ALL_DATABASE_FILES_$(date +%Y-%m-%d-%H.%M.%S).tar.gz /RACKBACKUP/BACKUP/DAILY/TEMP/ $SOURCE > $OUTPUT 2>$ERROR
cat $EMAIL | mail -a "$OUTPUT" -a"$ERROR" -s " BACKUP COMPLETE for MYSQLDUMP AND FILES `date` " $EMAIL_PERSON_2,$EMAIL_PERSON_1,$EMAIL_PERSON_3


#service mysqld start
/usr/bin/find /RACKBACKUP/BACKUP/DAILY/ -type f -mtime +8 -exec rm -f {} \;
